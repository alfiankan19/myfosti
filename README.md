WEB PORTOFOLIO FOSTI | FOSTI PROJECT HUB

Fitur :

- [x] Google Auth 
- [x] Visual Editorjs
- [x] Like
- [x] Search
- [ ] Comment
- [ ] Share
- [ ] SEO

Infra :
- Codeigniter sebagai backend standalone
- Frontend campuran native Javascript & Jquery

Cara SetUp Standar :
1. Copy Folder public_html ke folder host yang dipakai
2. Setup database import fosti.sql
3. Config db di application/config/database.php

Cara setup dengan docker :
1. buka terminal dalam root Folder
2. ketik `docker-compose up -d`
3. import fosti.sql ke http://host/phpmyadmin
4. akses url di root host port 80 atau http://host

Team :
- Sofyan | Project Manager
- Alfiankan | Maintener
- Bintang | Maintener
- Afifah | Maintener 
