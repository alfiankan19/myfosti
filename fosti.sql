-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 03, 2020 at 12:44 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fosti`
--

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `konten` text NOT NULL,
  `team` text NOT NULL,
  `stack` text NOT NULL,
  `repo` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `telp` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `visibility` int(2) NOT NULL,
  `gambar` text NOT NULL,
  `likes` text,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `userid`, `judul`, `kategori`, `konten`, `team`, `stack`, `repo`, `email`, `telp`, `view`, `visibility`, `gambar`, `likes`, `deskripsi`) VALUES
(11, '104816483680844774294', 'Robot terbang Arduino', 'Website', '{"time":1596414965738,"blocks":[{"type":"header","data":{"text":"JUDUL","level":3}},{"type":"paragraph","data":{"text":"adadadadadad"}},{"type":"paragraph","data":{"text":"addadadada"}},{"type":"paragraph","data":{"text":"ad"}},{"type":"header","data":{"text":"gggggg","level":3}},{"type":"header","data":{"text":"sssss","level":3}},{"type":"header","data":{"text":"shhhhhhh","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596378663.jpg"},"caption":"ini pertemuan fostu<br>","withBorder":false,"stretched":false,"withBackground":false}},{"type":"list","data":{"style":"ordered","items":["ESP32","breadboaed","sensor"]}},{"type":"paragraph","data":{"text":"PROGRESS KAMI"}},{"type":"checklist","data":{"items":[{"text":"mulai","checked":true},{"text":"proses","checked":true},{"text":"selesai","checked":false},{"text":"lanjutkan","checked":false}]}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Xphp phpinfo(); ?X","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Developer","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"}]', '[{"row":0,"stack":"C"},{"row":1,"stack":"Python"},{"row":2,"stack":"Laravel"},{"row":3,"stack":"Vue"},{"row":4,"stack":"Golang"},{"row":5,"stack":"Angular"},{"row":6,"stack":"C++"}]', '[{"row":0,"stack":"Gitlab","url":"https://gitlab.com/jikodafssdadafdfadfadfad"}]', 'a@a.com', '242646226246', 249, 1, '{"1":"1596293553672.jpg","2":"1596293558901.jpg","3":"1596293563951.jpg"}', '[]', 'Robot uni kecil manis dan pintar sekali'),
(12, '104816483680844774294', 'Robot api arduino', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 8, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Malam keakraban untuk anggota baru Fosti 2019 selama 3 hari, ada berbagai kegiatan yang nantinya akan membantu dalam kepengurusan Fosti yang selanjutnya.'),
(13, '104816483680844774294', 'Robot api arduino 2', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Malam keakraban untuk anggota baru Fosti 2019 selama 3 hari, ada berbagai kegiatan yang nantinya akan membantu dalam kepengurusan Fosti yang selanjutnya.'),
(14, '104816483680844774294', 'Robot api arduino 3', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 4, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Malam keakraban untuk anggota baru Fosti 2019 selama 3 hari, ada berbagai kegiatan yang nantinya akan membantu dalam kepengurusan Fosti yang selanjutnya.'),
(15, '104816483680844774294', 'Robot api arduino 4', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Malam keakraban untuk anggota baru Fosti 2019 selama 3 hari, ada berbagai kegiatan yang nantinya akan membantu dalam kepengurusan Fosti yang selanjutnya.'),
(16, '104816483680844774294', 'Robot api arduino 5', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 4, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Malam keakraban untuk anggota baru Fosti 2019 selama 3 hari, ada berbagai kegiatan yang nantinya akan membantu dalam kepengurusan Fosti yang selanjutnya.'),
(17, '104816483680844774294', 'Robot api arduino 6', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Malam keakraban untuk anggota baru Fosti 2019 selama 3 hari, ada berbagai kegiatan yang nantinya akan membantu dalam kepengurusan Fosti yang selanjutnya.'),
(18, '104816483680844774294', 'Robot api arduino 7', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(19, '104816483680844774294', 'Robot api arduino 8', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', 'Scroll the document to position "300" horizontally and "500" vertically:'),
(20, '104816483680844774294', 'Robot api arduino 33', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(21, '104816483680844774294', 'Robot api arduino 21', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(22, '104816483680844774294', 'Robot api arduino 32', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(23, '104816483680844774294', 'Robot api arduino 443', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 5, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(24, '104816483680844774294', 'Robot api arduino 512', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(25, '104816483680844774294', 'Robot api arduino 611', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 3, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(26, '104816483680844774294', 'Robohox', 'AI', '{"time":1596342574129,"blocks":[{"type":"code","data":{"code":"const express = require("express");\nconst bodyParser = require("body-parser");\nconst cors = require("cors");\n\nconst app = express();\n\nvar corsOptions = {\n  origin: "http://localhost:8081"\n};\n\napp.use(cors(corsOptions));\n\n// parse requests of content-type - application/json\napp.use(bodyParser.json());\n\n// parse requests of content-type - application/x-www-form-urlencoded\napp.use(bodyParser.urlencoded({ extended: true }));\n\n// simple route\napp.get("/", (req, res) => {\n  res.json({ message: "Welcome to bezkoder application." });\n});\n\n// set port, listen for requests\nconst PORT = process.env.PORT || 8080;\napp.listen(PORT, () => {\n  console.log(`Server is running on port ${PORT}.`);\n});\n"}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projet Manager","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Minntener","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"}]', '[{"row":0,"stack":"Codeigniter"},{"row":1,"stack":"Python"}]', '[{"row":0,"stack":"Gitlab","url":"https://gitlab.com/afmkasmf"}]', 'alfiankan19@gmail.com', '0987364724', 3, 0, '{"1":"1596340566432.jpg","2":"1596340573505.jpg","3":"1596340580565.jpg"}', '[]', ''),
(27, '104816483680844774294', 'Robot api arduino 865', 'Robotik', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Projec Manager"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Stack Developer"}]', '[{"row":0,"stack":"Arduino"},{"row":1,"stack":"Django"}]', '[{"row":0,"stack":"Gitlab","url":"gitlku"}]', 'j@ww.com', '987890', 18, 0, '{"1":"1596294806708.jpg","2":"1596294811054.jpg","3":"1596294814876.jpg"}', '[]', ''),
(29, '104816483680844774294', 'Robot terbang Arduino 90', 'Website', '{"time":1596333959511,"blocks":[{"type":"header","data":{"text":"judullll","level":3}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596333955.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}}],"version":"2.18.0"}', '[{"row":0,"nama":"andika","id":"2","tugas":"Xphp phpinfo(); ?X","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Developer","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"}]', '[{"row":0,"stack":"C"},{"row":1,"stack":"Python"}]', '[{"row":0,"stack":"Gitlab","url":"https://gitlab.com/jikodafssdadafdfadfadfad"}]', 'a@a.com', '242646226246', 21, 1, '{"1":"1596293553672.jpg","2":"1596293558901.jpg","3":"1596293563951.jpg"}', '[]', ''),
(30, '114415605257263837049', 'Faker JS', 'Program', '{"time":1596382671904,"blocks":[{"type":"header","data":{"text":"FAKER JS","level":3}},{"type":"paragraph","data":{"text":"adalah framework javascript sederhana"}},{"type":"image","data":{"file":{"url":"http://localhost/api/upload/1596382483.jpg"},"caption":"","withBorder":false,"stretched":false,"withBackground":false}},{"type":"paragraph","data":{"text":"d"}}],"version":"2.18.0"}', '[{"row":0,"nama":"Podkestan podcast","id":"4","tugas":"Developer","pp":"https://lh3.googleusercontent.com/a-/AOh14GgOwv8xlaR_Ec__ntcRmgPzR1wkczIZlnhTPOnc"},{"row":1,"nama":"alfiankan","id":"1","tugas":"Maintener","pp":"https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c"}]', '[{"row":1,"stack":"Vue"},{"row":2,"stack":"Bootstrap"},{"row":3,"stack":"Python"},{"row":4,"stack":"Golang"}]', '[{"row":0,"stack":"Gitlab","url":"https://gitlab.com/myrepo"}]', 'aer@f.com', '837827897832', 11, 0, '{"1":"1596382348946.jpg","2":"1596382355394.jpg","3":"1596382363858.jpg"}', '[]', 'adalah javascript untuk generate fake data untuk guna pengetesan aplikasi');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) NOT NULL,
  `token` varchar(500) NOT NULL,
  `role` int(5) NOT NULL,
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `openid`, `token`, `role`, `image`, `name`) VALUES
(1, '104816483680844774294', 'ya29.a0AfH6SMArIfMKxnMhSsstdJTqd427A0Vnpulperszy8itQ-ugGg1o4Tu3QtJDztXosNSp1uGeODJsEby6ozJGfd6MKUq2y6a-QEAgmPWyBQhCfpZYX6MRHcn2123Dga2dlt9MunbNInN_XaxQFvEn7xgpOhtvK2qIXdZMtg', 1, 'https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c', 'alfiankan'),
(2, '10481648368084477429432', 'ya29.a0AfH6SMArIfMKxnMhSsstdJTqd427A0Vnpulperszy8itQ-ugGg1o4Tu3QtJDztXosNSp1uGeODJsEby6ozJGfd6MKUq2y6a-QEAgmPWyBQhCfpZYX6MRHcn2123Dga2dlt9MunbNInN_XaxQFvEn7xgpOhtvK2qIXdZMtg', 1, 'https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c', 'andika'),
(3, '10481648368084477429432ad', 'ya29.a0AfH6SMArIfMKxnMhSsstdJTqd427A0Vnpulperszy8itQ-ugGg1o4Tu3QtJDztXosNSp1uGeODJsEby6ozJGfd6MKUq2y6a-QEAgmPWyBQhCfpZYX6MRHcn2123Dga2dlt9MunbNInN_XaxQFvEn7xgpOhtvK2qIXdZMtg', 1, 'https://lh3.googleusercontent.com/a-/AOh14GhTMQsR1GwiDeCiTZOLTFZiLRb5HBQe1DVAKX7c', 'Clarissa\r\n'),
(4, '114415605257263837049', 'ya29.a0AfH6SMDM6Z-Cy94Y9zqv9qv2w_R-5OWZnrMqe3Atnrh1y7__5KGT0f5CEjL4-pYpAQbPIUsljIjoVtGslY9HQVfs1aim6p6yhD1JZg6i4GYa8K4I6K8IX5KRDpwrmwaVxoKHvY8hclpCl2Rw3_cMjgS6EXDcMH-IDRcaRQ', 1, 'https://lh3.googleusercontent.com/a-/AOh14GgOwv8xlaR_Ec__ntcRmgPzR1wkczIZlnhTPOnc', 'Podkestan podcast');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;