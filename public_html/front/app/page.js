"use strict";
var lastRow=0;
class pageControl{

    finish(){
        setTimeout(function(){ 
            window.document.getElementById('loading-kiri').style.display = 'none'; 
        }, 2000);
    }
    kanan(){
        console.log(this.datapage);
    }

    judul(judul){
        $('#judul').text(judul);
    }
    kategori(kategori){
        $('#kategori').text(kategori);
    }

    stack(stack){

        var liststack = JSON.parse(stack);
        for(var x=0;x<liststack.length;x++){
            console.log(liststack[x]);
            $('#_liststack').append(' <div class="col-xs-4">'+
            '                        <img style="width:50px;padding-right:5px;padding-left:5px;padding-top:5px;padding-bottom:5px;" data-toggle="tooltip" title="'+liststack[x].stack+'" class="img-thumbnail" src="'+logopemograman[liststack[x].stack]+'" alt="">'+
            '                    </div>');
        }
   
    }

    // stack(stack){

    //     var liststack = JSON.parse(stack);
    //     for(var x=0;x<liststack.length;x++){
    //         console.log(liststack[x]);
    //         $('#_liststack').append('<div class="blog-comments__item d-flex p-3">'+
    //         '                      <div class="blog-comments__avatar mr-3">'+
    //         '                        <img src="'+logopemograman[liststack[x].stack]+'" alt="User avatar" /> </div>'+
    //         '                      <div class="blog-comments__content">'+
    //         '                        <div class="blog-comments__meta text-muted">'+
    //         '                          <p class="text-secondary" style="margin-top:5px;">'+liststack[x].stack+'</p>'+
    //         '                        </div>'+
    //         '                      </div>'+
    //         '                    </div>');
    //     }

    // }

    team(teamm){
        var listteam = JSON.parse(teamm);
        console.log(listteam)
        for(var x=0;x<listteam.length;x++){
            console.log(listteam[x]);
            $('#_listteam').append('<div class="blog-comments__item d-flex p-3">'+
            '                      <div class="blog-comments__avatar mr-3">'+
            '                        <img src="images/avatars/1.jpg" alt="User avatar" /> </div>'+
            '                      <div class="blog-comments__content">'+
            '                        <div class="blog-comments__meta text-muted">'+
            '                          <a class="text-secondary" href="#">'+listteam[x].nama+'</a>'+
            '                          <br><span class="text-muted">'+listteam[x].tugas+'</span>'+
            '                        </div>'+
            ''+
            '                      </div>'+
            '                    </div>');
        }
    }

    repo(repos){
        var repository = JSON.parse(repos);
        console.log(repository)
        for(var x=0;x<repository.length;x++){
            console.log(repository[x]);
            $('#_listrepo').append('<div class="blog-comments__item d-flex p-3">'+
            '                      <div class="blog-comments__avatar mr-3">'+
            '                        <img src="'+logorepo[repository[x].stack]+'" alt="User avatar" /> </div>'+
            '                      <div class="blog-comments__content">'+
            '                        <div class="blog-comments__meta text-muted">'+
            '                          <a class="text-secondary" href="#">'+repository[x].stack+'</a>'+
            '                          <br><a href="'+repository[x].url+'" target="_blank" class="text-muted">'+repository[x].url+'</a>'+
            '                        </div>'+
            ''+
            '                      </div>'+
            '                    </div>');
        }
    }

    kontak(email,phone){
        $('#_vemail').text(email);
        $('#_vphone').text(phone);
    }

    blockDetail(data){
        console.log(JSON.parse(data));

        var toFront = JSON.parse(data).blocks;
        var allList=0;
        var allCekLis=0;
        var x =0;
        for (let i = 0; i < toFront.length; i++) {
            
            console.log(toFront[i]);
            switch (toFront[i].type) {
                case 'header':
                    $('#detail-block').append('<h2>'+toFront[i].data.text+'</h2>');
                  break;
                case 'image':
                    $('#detail-block').append('<br><center><img class="img-fluid" style="max-width: 80%;margin-bottom:10px;" src="'+toFront[i].data.file.url+'" alt="'+toFront[i].data.caption+'"></center>');
                  break;
                case 'paragraph':
                    $('#detail-block').append('<p>'+toFront[i].data.text+'</p>');
                  break;
                case 'list':
                    
                    console.log('iniiii lisytttt');
                    //console.log(toFront[i].data.items);
                    
                    var listData = toFront[i].data.items;
                    if(listData){
                        $('#detail-block').append('<br><ol id="list-front-'+allList+'"></ol>');
                        console.log(listData);
                        for (x = 0; x < listData.length; x++) {
                        console.log('itemmm'+listData[x]);
                            $('#list-front-'+allList).append('<li>'+listData[x]+'</li>');
                        }
                        
                    }
                    break;
                case 'checklist':
                    console.log(toFront[i].data.items);
                    var ceklisData = toFront[i].data.items;
                    $('#detail-block').append('<div id="ceklis-front-'+allCekLis+'" class="cdx-block cdx-checklist"></div>');

                    for (x = 0; x < ceklisData.length; x++) {
                        console.log(ceklisData[x].text);
                        if(ceklisData[x].text){
                            if(ceklisData[x].checked){
                                $('#ceklis-front-'+allList).append('<div class="cdx-checklist__item cdx-checklist__item--checked">'+
                                '<span class="cdx-checklist__item-checkbox"></span>'+
                                '<div class="cdx-checklist__item-text" contenteditable="true">'+ceklisData[x].text+'</div>'+
                                '</div>');
                            }else{
                                $('#ceklis-front-'+allList).append('<div class="cdx-checklist__item">'+
                                '<span class="cdx-checklist__item-checkbox"></span>'+
                                '<div class="cdx-checklist__item-text" contenteditable="true">'+ceklisData[x].text+'</div>'+
                                '</div>');
                            }
                        }
                    }
                // <div class="cdx-block cdx-checklist">
                //     <div class="cdx-checklist__item cdx-checklist__item--checked">
                //         <span class="cdx-checklist__item-checkbox"></span>
                //         <div class="cdx-checklist__item-text" contenteditable="true">mulai</div>
                //     </div>
                //     <div class="cdx-checklist__item cdx-checklist__item--checked">
                //         <span class="cdx-checklist__item-checkbox"></span>
                //         <div class="cdx-checklist__item-text" contenteditable="true">proses</div>
                //     </div>
                //     <div class="cdx-checklist__item">
                //         <span class="cdx-checklist__item-checkbox"></span>
                //         <div class="cdx-checklist__item-text" contenteditable="true">selesai</div>
                //     </div>
                // </div>
              }
            
        }
        setTimeout(function(){ 
            window.document.getElementById('loading-atas').style.display = 'none'; 
        }, 3000);
       
    }

    viewjum(view){
        $('#_jview').text(view);
        
    }
    
    viewLike(){
        if(likePool){
            $('#_lview').text(likePool.length);
            for (let x = 0; x < likePool.length; x++) {
                if(likePool[x]==sesiUser){
                    window.document.getElementById('btn-respect').style.backgroundColor = 'blue';
                    isLike=1;
                }else{
                    window.document.getElementById('btn-respect').style.backgroundColor = '#5a6169';
                    isLike=0;
                }
            }
        }

    }

    detail(data){
        
    }
    komen(){

    }

    vgambar(data){
        var dgambar = JSON.parse(data);
        $('#caro-1').append('<div class="item"><a href="http://localhost/api/upload/'+dgambar[1]+'"><img style="border-radius: 15px;" src="http://localhost/api/upload/'+dgambar[1]+'"></a><h4></h4></div>');
        $('#caro-1').append('<div class="item"><a href="http://localhost/api/upload/'+dgambar[2]+'"><img style="border-radius: 15px;" src="http://localhost/api/upload/'+dgambar[2]+'"></a><h4></h4></div>');
        $('#caro-1').append('<div class="item"><a href="http://localhost/api/upload/'+dgambar[3]+'"><img style="border-radius: 15px;" src="http://localhost/api/upload/'+dgambar[3]+'"></a><h4></h4></div>');
        $('#caro-1').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
        $('.zoom-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function(item) {
                    return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                }
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function(element) {
                    return element.find('img');
                }
            }
            
        });
  
    }

    lainya(kategori,judul){
        $.ajax({

            type : 'POST',
            url  : mainUrl+'/api/listall',
            dataType: 'JSON',
            data:{limit:5,mulai:0,keyword:'',kategori:kategori},
            beforeSend: function(){
               
            },
            success :  function(response){
                
                console.log(response);
                
                for (let c = 0; c < response.length; c++) {
                    
                    if(response[c].judul!==judul){
                        $('#lainya-head').html('<div class="page-header row no-gutters py-4">'+
                        '<div class="col-12 col-sm-4 text-center text-sm-left mb-0">'+
                          '<span class="text-uppercase page-subtitle">Untuk Kamu</span>'+
                          '<h3 class="page-title">Lihat Juga</h3>'+
                        '</div>'+
                      '</div>');
                        $('#lainya').append('<div class="card card-small card-post card-post--aside card-post--1">'+
                        '<div class="card-post__image" style="background-image: url(\''+mainUrl +'/api/upload/'+ JSON.parse(response[c].gambar)[1]+'\');">'+
                          '<div class="card-post__author d-flex">'+
                          '</div>'+
                        '</div>'+
                        '<div class="card-body">'+
                          '<h5 class="card-title">'+
                            '<a class="text-fiord-blue" href="#">'+response[c].judul+'</a>'+
                          '</h5>'+
                          '<p class="card-text d-inline-block mb-3">'+response[c].deskripsi+'</p>'+

                        '</div>'+
                        '            <div class="card-footer">'+
                        '              <button type="button" onclick="openPage(\''+response[c].judul.replace(/\s+/g,'-').toLowerCase()+'\');" class="btn btn-secondary">Detail</button>'+
                        '            </div>'+
                      '</div><br>');
                         
                    }
                    
                }
                
         
            }
        
        });
    }
    init(data){
        console.log(data);
        this.vgambar(data.gambar);
        this.judul(data.judul);
        this.kategori(data.kategori);
        this.stack(data.stack);
        this.team(data.team);
        this.repo(data.repo);
        this.kontak(data.email,data.telp);
        this.viewLike(data.like);
        this.viewjum(data.view);
        this.blockDetail(data.konten);
        this.lainya(data.kategori,data.judul);
        this.finish();
        
    }
}


function colormode(){
 
        window.document.body.style.backgroundColor = 'rgb(21, 32, 43)';
    
}


function respectLike(){

    if(sesiUser!=null){
        if(isLike==0){
            //like
            console.log("LIKE");
            likePool.push(sesiUser);
            isLike=1;
            new pageControl().viewLike();
        }else{
            console.log("DISLIKE");
            likePool.remove(sesiUser);
            isLike=0;
            new pageControl().viewLike();
        }
        var likestr = JSON.stringify(likePool);
        $.ajax({
    
            type : 'POST',
            url  : mainUrl+'/api/like',
            data : {
                like:likestr,
                id:pageId,
                openid:sesiUser,
                token:sesiToken
            },
            dataType: 'JSON',
            beforeSend: function(){
               
            },
            success :  function(response){
                
              
            }
        
        });
    }else{
        $('#loginModal').modal('show');
    }


    
}

class Homeapp{
    finish(){
        if(lastRow==8){
            for (let x = 0; x < lastRow.length; x++) {
                setTimeout(function(){ 
                    window.document.getElementById('loading-card-'+lastRow).style.display = 'none'; 
                }, 2000);
                
            }
        }else{
            var loadmore=lastRow-8;
            for (let x=loadmore;x < lastRow.length; x++) {
                setTimeout(function(){ 
                    window.document.getElementById('loading-card-'+lastRow).style.display = 'none'; 
                }, 2000);
                
            }
        }

    }
    all(data){

        for (let x = 0; x < data.length; x++) {
            var iconStack = JSON.parse(data[x].stack);
            console.log(iconStack);

            $('#home-all').append('<div class="col-lg-3 col-md-6 col-sm-12 mb-4">'+
            '          <div class="card card-small card-post card-post--1 h-100">'+
            '            <div class="card-post__image" style="background-image: url(\''+mainUrl +'/api/upload/'+ JSON.parse(data[x].gambar)[1]+'\');">'+
            '              <a href="#" class="card-post__category badge badge-pill badge-dark">'+data[x].kategori+'</a>'+

            '            </div>'+
            '            <div class="card-body">'+
            '              <h5 class="card-title">'+
            '                <a class="text-fiord-blue" href="#">'+data[x].judul+'</a>'+
            '              </h5>'+
            '              <div class="container"><div class="row" id="_liststack-'+lastRow+'"></div></div>'+
            '                                                                        <p>'+data[x].deskripsi.split(" ").slice(0,20).toString().replace(/,/g, ' ')+'...</p>'+
            '            </div>'+
            '            <div class="card-footer">'+
            '              <button type="button" onclick="openPage(\''+data[x].judul.replace(/\s+/g,'-').toLowerCase()+'\');" class="btn btn-secondary">Detail</button>'+
            '            </div>'+
            '          </div>'+
            '<div id="loading-card-'+lastRow+'" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:#e3e5e8" ><div style="  z-index:1;position:relative;top:0;left:0;width:100%;height:100%;" class="shine-me"></div></div>'+
            '        </div>');

            for (let i = 0; i < iconStack.length; i++) {
                console.log(iconStack[x]);
                $('#_liststack-'+lastRow).append(' <div class="col-xs-4">'+
                '                        <img style="width:30px;padding-right:5px;padding-left:5px;padding-top:5px;padding-bottom:5px;" data-toggle="tooltip" title="'+iconStack[i].stack+'" class="img-thumbnail" src="'+logopemograman[iconStack[i].stack]+'" alt="">'+
                '                    </div>');
            }
            window.document.getElementById('loading-card-'+lastRow).style.display = 'none'; 
            lastRow++
            console.log(lastRow);
            
        }

    }
    init(data){

        this.all(data);
        $('.dropdown-item').click(function(){

            console.log($(this).text());
            lastRow=0;
            mainKategori=$(this).text();
            mainKeyword=$('#search').val();
            new Depan().homePage();
        
        });

    }

}

$(window).scroll(function() {
    if($(window).scrollTop() == $(document).height() - $(window).height()) {
           console.log('mentok');
           console.log(mainKeyword);
           $.ajax({

            type : 'POST',
            url  : mainUrl+'/api/listall',
            dataType: 'JSON',
            data:{limit:lastRow+8,mulai:lastRow,keyword:mainKeyword,kategori:mainKategori},
            beforeSend: function(){
               
            },
            success :  function(response){
                
                console.log(response);
                new Homeapp().init(response);
         
            }
        
        });
    }
});

function openPage(iden){
    console.log(iden);
    history.pushState({page: 1}, iden, iden);
    new Render().engine(use._portofolio);
    $.ajax({

        type : 'POST',
        url  : mainUrl+'/api/getpage/',
        data : {url:iden},
        dataType: 'JSON',
        beforeSend: function(){
           
        },
        success :  function(response){
            
            console.log(response.error);
            if(response.error!='404'){
                likePool=JSON.parse(response.likes);
                $('title').text(response.judul+' | Fosti Project Hub');
                pageId=response.id;
                new pageControl().init(response);
            }else{
                window.location.href = '/';
            }
        }
    
    });
}
window.onpopstate = function(e) {
    new Depan().homePage();
    lastRow=0;
};

function cariCari(){
    lastRow=0;
    mainKeyword=$('#search').val();
    new Depan().homePage();
}

