//We dedicated To FOSTI
//Portofolio WEB Team
//MIT license authored by alfiankan19
"use strict";

//urllllllllllllll

var mainUrl = window.location.origin;

const use={
    baseUri:window.location.href,
    root:window.document.getElementById('root'),
    top:window.document.getElementById('top'),
    _auth:'front/app/page/auth.html',
    _portofolio:'front/app/page/portofolio.html',
    _dashboard:'front/app/page/dashboard.html',
    _akses:'front/app/page/akses.html',
    _login:'front/app/page/login.html',
    _home:'front/app/page/home.html',
    _top:'front/app/page/top.html',
    Obj_btnlogin:window.document.getElementById('btn-login'),
    Obj_userinfo:window.document.getElementById('userinfo')
}
var firebaseConfig = {
    apiKey: "AIzaSyC1JTzAEO_PlEzjgOMGv69POwaIXKlJZQE",
    authDomain: "fosti-a6771.firebaseapp.com",
    databaseURL: "https://fosti-a6771.firebaseio.com",
    projectId: "fosti-a6771",
    storageBucket: "fosti-a6771.appspot.com",
    messagingSenderId: "145490175800",
    appId: "1:145490175800:web:d47d24712719ae302243cd",
    measurementId: "G-WFM8M7RLL7"
  };
console.log(use.baseUri);
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
//declare var
var sesiUser,sesiToken;
var dataPortofolio;
var status='logout';
var userid;
var pp = sessionStorage.getItem("image");
var likePool;
var isLike=0;
var pageId;
var role=0;
var mainKeyword='';
var mainKategori='';
//end declare

class Render{
    engine(page){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", page, false);
        xmlhttp.send();
        use.root.innerHTML = xmlhttp.responseText;

        //$('#root').load(page);
    }

    engineTop(page){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", page, false);
        xmlhttp.send();
        use.top.innerHTML = xmlhttp.responseText;
    }

    root(page){
        this.engineTop(use._top);
        this.engine(page);
    }

    url(){
        return window.location.href.substring(window.location.href.lastIndexOf('/') + 1)
    }
}


class Auth extends Render{
    belumLogin() {
        console.log("belum Login");
        this.root(use._auth);
    }
    sudahLogin(user,token) {
        this.root();
    }



    cekLogin(){
        console.log('cek login api');
        sesiUser = sessionStorage.getItem("sessionKey");
        sesiToken = sessionStorage.getItem("Token");
        console.log(sesiUser);
        $.ajax({

            type : 'POST',
            url  : mainUrl+'/api/auth',
            data : {
                openid:sesiUser,
                token:sesiToken
            },
            dataType: 'JSON',
            beforeSend: function(){
               
            },
            success :  function(response){
                
                console.log(response.status);
               
                status=response.status;
                role=response.role;
                sessionStorage.setItem("Role", response.role);
               
                if(status=='login'){
                    window.document.getElementById('btn-login').style.display = 'none';
                    window.document.getElementById('userinfo').style.display = 'block';
                    window.document.getElementById('pp').src = pp;
                    window.document.getElementById('username').innerHTML = sessionStorage.getItem("Username");
                    return true;
                }else{

                    if(sesiUser!=null){
                       console.log('mendaftar');
                       daftarin(); 
                    }

                    window.document.getElementById('btn-login').style.display = 'block';
                    window.document.getElementById('userinfo').style.display = 'none';
                    return false;
                }
                
            }
        
          });


        // if(sesiUser==null){
        //     return false;
        // }else{
        //     //status='login';
        //     return true;
        // }
    }
}

function daftarin(){
    $.ajax({

        type : 'POST',
        url  : mainUrl+'/api/daftar',
        data : {
            openid:sesiUser,
            token:sesiToken,
            image:sessionStorage.getItem("image"),
            name:sessionStorage.getItem("Username")
        },
        dataType: 'JSON',
        beforeSend: function(){
           
        },
        success :  function(response){
            if(response.status=='oke'){
                window.location.href = '/';
            }
        }
    
      });
}

//cek user telah login in session storage untuk di dashboard

class Depan extends Render{

    homePage(){
        console.log("welcome home baby");
        this.root(use._home);
        window.scrollTo(500, 0); 
        $('title').text('Fosti Project Hub');
        $.ajax({

            type : 'POST',
            url  : mainUrl+'/api/listall',
            dataType: 'JSON',
            data:{limit:8,mulai:0,keyword:mainKeyword,kategori:mainKategori},
            beforeSend: function(){
               
            },
            success :  function(response){
                
                console.log(response);
                new Homeapp().init(response);
         
            }
        
        });
    }

    portofolioPage(url){
        this.root(use._portofolio);
        //ambil portofolio datanya
        
        console.log(url);
        $.ajax({

            type : 'POST',
            url  : mainUrl+'/api/getpage/',
            data : {url:url},
            dataType: 'JSON',
            beforeSend: function(){
               
            },
            success :  function(response){
                
                console.log(response.error);
                if(response.error!='404'){
                    likePool=JSON.parse(response.likes);
                    $('title').text(response.judul+' | Fosti Project Hub');
                    pageId=response.id;
                    new pageControl().init(response);
                }else{
                    window.location.href = '/';
                }
            }
        
        });
        
    }

    page(url){
        console.log(url);
        url==''?this.homePage():this.portofolioPage(url);
    }

}

class Dashboard extends Render{

    permisi(){
        //memberikan izin dari role
        return 1;
    }

    page(){
        if(sessionStorage.getItem("Role")==1){
             this.root(use._dashboard);
        }else{
            
                this.root(use._akses);
          
            
        }
    }

}


















const belakang = new Dashboard();
const depan = new Depan();
const render = new Render();
const auth = new Auth();
render.url()=='dashboard' ? belakang.page():depan.page(render.url());
console.log(auth.cekLogin());





if(status=='login'){
    window.document.getElementById('btn-login').style.display = 'none';
    window.document.getElementById('userinfo').style.display = 'block';
    window.document.getElementById('pp').src = pp;
}else{
    window.document.getElementById('btn-login').style.display = 'block';
    window.document.getElementById('userinfo').style.display = 'none';
}



//firebase area -------------------------------------------------------------------------------
firebase.initializeApp(firebaseConfig);

var ui = new firebaseui.auth.AuthUI(firebase.auth());
    var uiConfig = {
        callbacks: {
          signInSuccessWithAuthResult: function(authResult, redirectUrl) {
            // User successfully signed in.
            // Return type determines whether we continue the redirect automatically
            // or whether we leave that to developer to handle.
            console.log("login oke");
      
            saveLogin(authResult);

            // Store

      
          },
          uiShown: function() {
            // The widget is rendered.
            // Hide the loader.
            document.getElementById('loader').style.display = 'none';
          }
        },
        // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
        signInFlow: 'popup',
        signInSuccessUrl: '<url-to-redirect-to-on-success>',
        signInOptions: [
          // Leave the lines as is for the providers you want to offer your users.
          firebase.auth.GoogleAuthProvider.PROVIDER_ID,
       
        ],
        // Terms of service url.
        tosUrl: '<your-tos-url>',
        // Privacy policy url.
        privacyPolicyUrl: '<your-privacy-policy-url>'
      };
      ui.start('#firebaseui-auth-container', uiConfig);

function saveLogin(authResult){
    sessionStorage.setItem("sessionKey", authResult.additionalUserInfo.profile.id);
           
    sessionStorage.setItem("Token", authResult.credential.accessToken);
    sessionStorage.setItem("Username", authResult.additionalUserInfo.profile.name);
    sessionStorage.setItem("image", authResult.additionalUserInfo.profile.picture);
    location.reload();
}

function logOut(){
    sessionStorage.clear();
    window.location.href = '/';
}