//We dedicated To FOSTI
//Portofolio WEB Team
//MIT license authored by alfiankan19
"use strict";

const dash={
    body:window.document.getElementById('dashboard-page'),
    btnBatal:window.document.getElementById('btn-batal'),
    btnBaru:window.document.getElementById('btn-baru'),
    dashTitle:window.document.getElementById('dashboard-title'),
    blokblok:$('#blok-blok')
}
//mode=0 ->new mode=1->edit
$('#kodeuser').text(sesiUser);
var mode=0;
var blockNum = 0;
var konten;
var editor;
var tesvar={};
function engine2(page){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", page, false);
    xmlhttp.send();
    dash.body.innerHTML = xmlhttp.responseText;
}
var gambarUtama={
    1:'',
    2:'',
    3:''
};

var datapf=[];
var pageId=0;
//tampil list
listpf();
function listpf(){
    //get data
    engine2('front/page/blank.html');
    $.ajax({

        type : 'POST',
        url  : mainUrl+'/api/listpf',
        data : {
            openid:sesiUser,
            token:sesiToken
        },
        dataType: 'JSON',
        beforeSend: function(){
           
        },
        success :  function(response){
            
            console.log(response);
            //init data
            for (let x = 0; x < response.length; x++) {
                datapf.push(response[x]);
                var thumb = JSON.parse(response[x].gambar);
            $('#dashboard-page').append('<div id="rowpf-'+response[x].id+'" class="blog-comments__item d-flex p-3">'+
            '    <div class="blog-comments__avatar mr-3">'+
            '      <img src="http://localhost/api/upload/'+thumb[1]+'" alt="User avatar" /> </div>'+
            '    <div class="blog-comments__content">'+
            '      <div class="blog-comments__meta text-muted">'+
            '        <a class="text-secondary" href="#">'+response[x].judul+'</a> - '+
            '        <a class="text-secondary" href="#">Terpublish</a>'+
            '      </div>'+
            '      <p class="m-0 my-1 mb-2 text-muted">Kategori - '+response[x].kategori+'</p>'+
            '      <div class="blog-comments__actions">'+
            '        <div class="btn-group btn-group-sm">'+
            '          <button type="button" class="btn btn-white">'+
            '            <span class="text-success">'+
            '              <i class="material-icons">check</i>'+
            '            </span> Unpublish </button>'+
            '          <button type="button" class="btn btn-white" id="deletePageBtn-'+response[x].id+'" onclick="deletePage('+response[x].id+')">'+
            '            <span class="text-danger">'+
            '              <i class="material-icons">clear</i>'+
            '            </span> Hapus </button>'+
            '          <button type="button" class="btn btn-white" onclick="editPf('+response[x].id+');">'+
            '            <span class="text-light">'+
            '              <i class="material-icons">more_vert</i>'+
            '            </span> Edit </button>'+
            '        </div>'+
            '      </div>'+
            '    </div>'+
            '  </div>');
     
            }
        }
    
    });

    //init data

}


var memberTeam=[];
var techStack=[];
var repository=[];
var ppMember={};
var memberRow=0;
var StackRow=0;
var RepoRow=0;
function getallUser(){
    //deklasari list team
    
   
    $.ajax({

        type : 'POST',
        url  : mainUrl+'/api/alluser',
        data : {
            openid:sesiUser,
            token:sesiToken
        },
        dataType: 'JSON',
        beforeSend: function(){
           
        },
        success :  function(response){
            
            console.log(response);
            var alluser_list='';
            for(var x=0;x<response.length;x++){
                alluser_list+= '<option value="'+response[x].id+'">'+response[x].name+'</option>';
                ppMember[response[x].id] = response[x].image;
            }
            $('#all-user').html(alluser_list);
            $('.js-example-basic-single').select2();

            $('#all-user').on('select2:select', function (e) {
                var data = e.params.data;
                console.log(data);
                $('#body-team').append('<tr id="tr-member-'+memberRow+'"><td><img style="width: 49px;" class="user-avatar rounded-circle mr-2" id="pp" src="'+ppMember[data.id]+'" alt="User Avatar">'+data.text+'</td><td><input onkeyup="setTugas('+memberRow+');" onkeydown="setTugas('+memberRow+');" id="tugas-'+memberRow+'" class="form-control form-control-lg mb-3" type="text" placeholder="Ex : Project Manager"></td><td><button onclick="delTeam('+memberRow+');" type="button" class="mb-2 btn btn-danger mr-2">X</button></td></tr>');
                var newMemberId={
                    row:memberRow,
                    nama:data.text,
                    id:data.id,
                    tugas:'',
                    pp:ppMember[data.id]
                }

                memberTeam.push(newMemberId);
      
                
                memberRow++
            });
            
        }
    
    });
    var alluser_stack='';
    for (var prop in logopemograman) {
        console.log(prop);
        alluser_stack+= '<option value="'+prop+'">'+prop+'</option>';
    }
    $('#tech-stack').html(alluser_stack);

    
    $('#tech-stack').on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);
        var newStack = {
            row:StackRow,
            stack:data.id
        }
        techStack.push(newStack);
        $('#body-stack').append('<tr id="tr-stack-'+StackRow+'"><td><img style="width: 49px;" class="user-avatar mr-2" id="stackpp" src="'+logopemograman[data.id]+'" alt="User Avatar"></td>'+data.text+'<td></td><td><button onclick="delStack('+StackRow+');" type="button" class="mb-2 btn btn-danger mr-2">X</button></td></tr>');
        StackRow++
    });

    var allrepo='';
    for (var prop in logorepo) {
        console.log(prop);
        allrepo+= '<option value="'+prop+'">'+prop+'</option>';
    }
    $('#list-repo').html(allrepo);

    
    $('#list-repo').on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);
        var newRepo = {
            row:RepoRow,
            stack:data.id,
            url:''
        }
        repository.push(newRepo);
        $('#body-repo').append('<tr id="tr-repo-'+RepoRow+'"><td><img style="width: 49px;" class="user-avatar mr-2" id="repopp" src="'+logorepo[data.id]+'" alt="User Avatar"> '+data.text+'</td><td><input onkeyup="repoUrl('+RepoRow+');" onkeydown="repoUrl('+RepoRow+');" id="repourl-'+RepoRow+'" class="form-control form-control-lg mb-3" type="text" placeholder="Ex : https://"></td><td><button onclick="delRepo('+RepoRow+');" type="button" class="mb-2 btn btn-danger mr-2">X</button></td></tr>');
        RepoRow++
    });

}

function setTugas(row){

    var formtugas = $('#tugas-'+row).val();
    for(var x=0;x<memberTeam.length;x++){
        if(memberTeam[x].row==row){
            memberTeam[x].tugas = formtugas;
            console.log(memberTeam[x]);
        }
    }

}

function repoUrl(row){

    var formurl = $('#repourl-'+row).val();
    for(var x=0;x<repository.length;x++){
        if(repository[x].row==row){
            repository[x].url = formurl;
        }
    }

}


function delTeam(row){
    console.log("menghapus anggota"+row)
    var memberTeamDelete=[];
    for(var c=0;c<memberTeam.length;c++){
        
        if(memberTeam[c].row!=row){
            memberTeamDelete.push(memberTeam[c]);
            console.log("yang masih ada"+memberTeam[c].nama)
        }
    }
    memberTeam = memberTeamDelete;
    
    $('#tr-member-'+row).detach();
}

function delStack(row){
    console.log("menghapus Stack"+row)
    var listStackDelete=[];
    for(var c=0;c<techStack.length;c++){
        
        if(techStack[c].row!=row){
            listStackDelete.push(techStack[c]);
            console.log("yang masih ada"+techStack[c].stack)
        }
    }
    techStack = listStackDelete;
    $('#tr-stack-'+row).detach();
}

function delRepo(row){
    console.log("menghapus Repo"+row)
    var listRepoDelete=[];
    for(var c=0;c<repository.length;c++){
        
        if(repository[c].row!=row){
            listRepoDelete.push(repository[c]);
            console.log("yang masih ada"+repository[c].stack)
        }
    }
    repository = listRepoDelete;
    $('#tr-repo-'+row).detach();
}


function initEditor(data={}){
    editor = new EditorJS({
        holderId:'editorjs',
        styles:'ce-inline-tool--active',
        tools: {
            header: {
              class: Header,
              config: {
                placeholder: 'Tambahkan Heade',
                levels: [2, 3, 4],
                defaultLevel: 3
              }
            },
            image: {
                class: ImageTool,
                config: {
                  endpoints: {
                    byFile: mainUrl+'/api/uploader2', // Your backend file uploader endpoint
                    byUrl: mainUrl+':8008/fetchUrl', // Your endpoint that provides uploading by Url
                  },
                  field:'gambar',
                  additionalRequestData:{
                      namafile:Date.now()
                  }
                }
            },
            Marker: {
                class: Marker,
                shortcut: 'CMD+SHIFT+M',
            },
            inlineCode: {
                class: InlineCode,
                shortcut: 'CMD+SHIFT+M',
            },
            //code: CodeTool,
            list: {
                class: List,
                inlineToolbar: true,
            },
            checklist: {
                class: Checklist,
                inlineToolbar: true,
              },
              embed: {
                class: Embed,
                inlineToolbar: true
              },
            
        },
        data:data,
        onReady: () => {console.log('Editor.js is ready to work!');editor.save().then((outputData) => {konten = outputData});},
   
        /**
         * onChange callback
         */
        onChange: () => {console.log('Now I know that Editor\'s content changed!');editor.save().then((outputData) => {konten = outputData});}
          
    });


}

function newBaru(){
    mode=0;
    dash.btnBaru.style.display = 'none';
    dash.btnBatal.style.display = 'block';
    dash.dashTitle.innerHTML = 'Portofolio Baru';
    engine2('front/app/page/new.html');
    

 
    initEditor();
    

    getallUser();

    
}

var dataToedit;
function newBatal(){
    dash.btnBaru.style.display = 'block';
    dash.btnBatal.style.display = 'none';
    dash.dashTitle.innerHTML = 'Daftar Portofolio';
    mode=0;
    var gambarUtama={
        1:'',
        2:'',
        3:''
    };
    dataToedit='';
    memberTeam=[];
    techStack=[];
    repository=[];
    ppMember={};
    memberRow=0;
    StackRow=0;
    RepoRow=0;
    datapf=[];
    listpf();

}

//menampung data team



var ignoreCek;
function editPf(id){
    
    mode=1;
    //newBaru();
    //initEditor(1);
    mode=1;
    dash.btnBaru.style.display = 'none';
    dash.btnBatal.style.display = 'block';
    dash.dashTitle.innerHTML = 'Portofolio Baru';
    engine2('front/app/page/new.html');
    getallUser();


    for (let x = 0; x < datapf.length; x++) {
       if(datapf[x].id == id){
           console.log(datapf[x]);
           pageId=datapf[x].id;
           ignoreCek= datapf[x].judul;
            if(datapf[x].konten){
                tesvar = JSON.parse(datapf[x].konten);

             
                initEditor(tesvar);
                
            
                
            }else{
                initEditor();
            }
            //set judul
            $('#form-judul').val(datapf[x].judul);
            //set team
            var editTeam = JSON.parse(datapf[x].team);
            console.log(editTeam);
            for (let i= 0; i < editTeam.length; i++) {
                console.log('ppmember---');
                console.log(ppMember);
                $('#body-team').append('<tr id="tr-member-'+memberRow+'"><td><img style="width: 49px;" class="user-avatar rounded-circle mr-2" id="pp" src="'+editTeam[i].pp+'" alt="User Avatar">'+editTeam[i].nama+'</td><td><input onkeyup="setTugas('+memberRow+');" onkeydown="setTugas('+memberRow+');" id="tugas-'+memberRow+'" class="form-control form-control-lg mb-3" type="text" placeholder="Ex : Project Manager" value="'+editTeam[i].tugas+'"></td><td><button onclick="delTeam('+memberRow+');" type="button" class="mb-2 btn btn-danger mr-2">X</button></td></tr>');
                var newMemberId={
                    row:memberRow,
                    nama:editTeam[i].nama,
                    id:editTeam[i].id,
                    tugas:editTeam[i].tugas,
                    pp:editTeam[i].pp
                }
    
                memberTeam.push(newMemberId);
                memberRow++
                
            }
            var editStack = JSON.parse(datapf[x].stack);
            for (let i= 0; i < editStack.length; i++) {
                var newStack = {
                    row:StackRow,
                    stack:editStack[i].stack
                }
                techStack.push(newStack);
                $('#body-stack').append('<tr id="tr-stack-'+StackRow+'"><td><img style="width: 49px;" class="user-avatar mr-2" id="stackpp" src="'+logopemograman[editStack[i].stack]+'" alt="User Avatar"></td>'+editStack[i].stack+'<td></td><td><button onclick="delStack('+StackRow+');" type="button" class="mb-2 btn btn-danger mr-2">X</button></td></tr>');
                StackRow++
            }

            var editRepo = JSON.parse(datapf[x].repo);
            for (let i= 0; i < editRepo.length; i++) {
                var newRepo = {
                    row:RepoRow,
                    stack:editRepo[i].stack,
                    url:editRepo[i].url
                }
                repository.push(newRepo);
                console.log(newRepo);
                $('#body-repo').append('<tr id="tr-repo-'+RepoRow+'"><td><img style="width: 49px;" class="user-avatar mr-2" id="repopp" src="'+logorepo[editRepo[i].stack]+'" alt="User Avatar"> '+editRepo[i].stack+'</td><td><input onkeyup="repoUrl('+RepoRow+');" onkeydown="repoUrl('+RepoRow+');" id="repourl-'+RepoRow+'" class="form-control form-control-lg mb-3" type="text" placeholder="Ex : https://" value="'+editRepo[i].url+'"></td><td><button onclick="delRepo('+RepoRow+');" type="button" class="mb-2 btn btn-danger mr-2">X</button></td></tr>');
                RepoRow++
            }
            $('#email').val(datapf[x].email);
            $('#telp').val(datapf[x].telp);

            //init edit gambar
            var editGambar = JSON.parse(datapf[x].gambar);

            for (var prop in editGambar) {
               
                console.log(editGambar[prop]);
                gambarUtama[prop] = editGambar[prop];
                $("#vgbr"+prop).attr('src',mainUrl+'/api/upload/'+editGambar[prop]);

            }

            $("#kategori").val(datapf[x].kategori);
            $('#deskripsi').val(datapf[x].deskripsi);


       }
    }
    

}


class pfControl{

    berhasil(id){
        $('#flashModal').modal('show');
        $('#flash-pesan').text('Page Terhapus');
        $('#deletePageBtn-'+id).html('<i class="material-icons">clear</i>'+'</span> Hapus </button>');
        $('#rowpf-'+id).detach();
    }
    gagal(id){
        $('#flashModal').modal('show');
        $('#flash-pesan').text('Page gagal Terhapus');
        $('#deletePageBtn-'+id).html('<i class="material-icons">clear</i>'+'</span> Hapus </button>');
    }
    init(id){
        $('#deletePageBtn-'+id).html('<img src="cdn/image/loading.svg" /> Menghapus');
    }
}


function deletePage(id){
    var r = confirm("Yakin Mau Menghapus?");
    if (r == true) {

        var delCon = new pfControl();
        delCon.init(id);
        $.ajax({

            type : 'POST',
            url  : mainUrl+'/api/delete',
            data : {
                openid:sesiUser,
                token:sesiToken,
                id:id
            },
            dataType: 'JSON',
            beforeSend: function(){
               
            },
            success :  function(response){
                
                if(response.delete=='1'){
                    delCon.berhasil(id);
                    
                }else{
                    delCon.gagal(id);
                }
         
                
            }
        
        });
    } else {
      //cancel
    }

}

function cekJudul(){
    var judulnya = $('#form-judul').val();
    $.ajax({

        type : 'POST',
        url  : mainUrl+'/api/cekjudul',
        data : {
            openid:sesiUser,
            token:sesiToken,
            judul:judulnya
        },
        dataType: 'JSON',
        beforeSend: function(){
           
        },
        success :  function(response){
            
            if(response.judul=='block'){
                if(mode==0){
                window.document.getElementById('form-judul').style.backgroundColor='red';
                $('#label-judul').text('Judul < Maaf Judul ini udah dipakai pakai yang lain >');
                }else{
                    //ignore ketika edit
                    if(judulnya==ignoreCek){

                    }else{
                        window.document.getElementById('form-judul').style.backgroundColor='red';
                        $('#label-judul').text('Judul < Maaf Judul ini udah dipakai pakai yang lain >');
                    }
                }
            }else{
                window.document.getElementById('form-judul').style.backgroundColor='';
                $('#label-judul').text('Judul');
            }
     
            
        }
    
    });
}




//fungsi upload gambar utama
function upGambar(no){
    console.log('Mengupload');
    
    $('#gbr'+no).trigger('click'); 
    $('#gbr'+no).change( function(event) {
        
        $("#vgbr"+no).attr('src','cdn/image/loading.svg');
        
        var file_data = $('#gbr'+no).prop('files')[0];
        var form_data = new FormData();
        form_data.append('gambar', file_data);
        form_data.append('namafile', Date.now());


        $.ajax({
            url: mainUrl+'/api/uploader',
            processData: false,
            contentType: false, 
            dataType: 'JSON',
            data: form_data,                         
            type: 'POST',
            success: function(response){
                console.log(response);

                gambarUtama[no] = response.upload_data.file_name;

                $("#vgbr"+no).attr('src',mainUrl+'/api/upload/'+response.upload_data.file_name);
            }
        });
        //$("#vgbr"+no).attr('src',URL.createObjectURL(event.target.files[0]));
    });

}





//menampung semua data 


var uploadData = {
    gambar:'',
    judul:'',
    kategori:'',
    team:'',
    stack:'',
    repo:'',
    email:'',
    telp:''
}

function simpan(){

    console.log(uploadData);
    //masukin data ahh
    console.log("val dari input"+$('#form-judul').val());


    uploadData['gambar'] = gambarUtama;
    uploadData['judul'] = $('#form-judul').val();
    uploadData['deskripsi'] = $('#deskripsi').val();
    uploadData['kategori'] = $('#kategori').val();
    uploadData['team'] = memberTeam;
    uploadData['stack'] = techStack;
    uploadData['repo'] = repository;
    uploadData['email'] = $('#email').val();
    uploadData['telp'] = $('#telp').val();
    console.log(uploadData);

    $.ajax({

        type : 'POST',
        url  : mainUrl+'/api/newpage',
        data : {
            openid:sesiUser,
            konten:JSON.stringify(konten),
            id:pageId,
            deskripsi:uploadData.deskripsi,
            token:sesiToken,
            gambar:JSON.stringify(uploadData.gambar),
            judul:uploadData.judul,
            kategori:uploadData.kategori,
            team:JSON.stringify(uploadData.team),
            stack:JSON.stringify(uploadData.stack),
            repo:JSON.stringify(uploadData.repo),
            email:uploadData.email,
            telp:uploadData.telp,
            mode:mode
        },
        dataType: 'JSON',
        beforeSend: function(){
           
        },
        success :  function(response){
            
            if(response.code=='200'){
                //alert('Berhasil Upload');

                $('#flashModal').modal('show');
                $('#flash-pesan').text("Berhasil");
            }else{
                window.document.getElementById('form-judul').style.backgroundColor='';
                $('#label-judul').text('Judul');
            }
     
            
        }
    
    });
    
}