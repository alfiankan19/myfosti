const logopemograman={
'Angular':'cdn/image/angular.png',
'C#':'cdn/image/c#.png',
'CSS':'cdn/image/css.png',     
'HTML':'cdn/image/html.png',  
'Laravel':'cdn/image/laravel.png',
'React':'cdn/image/react.png',
'Arduino':'cdn/image/arduino.png',
'C++':'cdn/image/c++.png',
'Django':'cdn/image/django.png',
'Java':'cdn/image/java.png',
'Lua':'cdn/image/lua.png',
'Vue':'cdn/image/vue.png',
'Bootstrap':'cdn/image/bootstrap.png',
'C':'cdn/image/c.png',
'PHP':'cdn/image/php.png',
'Codeigniter':'cdn/image/ci.png',
'Golang':'cdn/image/golang.png',
'Javascript':'cdn/image/js2.png',
'Python':'cdn/image/python.png'
}

const attrprog={
    'Angular':'cdn/image/angular.png',
    'C#':'cdn/image/c#.png',
    'CSS':'cdn/image/css.png',     
    'HTML':'cdn/image/html.png',  
    'Laravel':'cdn/image/laravel.png',
    'React':'cdn/image/react.png',
    'Arduino':'cdn/image/arduino.png',
    'C++':'cdn/image/c++.png',
    'Django':'Framework',
    'Java':'cdn/image/java.png',
    'Lua':'cdn/image/lua.png',
    'Vue':'cdn/image/vue.png',
    'Bootstrap':'cdn/image/bootstrap.png',
    'C':'cdn/image/c.png',
    'PHP':'cdn/image/php.png',
    'Codeigniter':'cdn/image/ci.png',
    'Golang':'cdn/image/golang.png',
    'Javascript':'cdn/image/js2.png',
    'Python':'cdn/image/python.png'
    }

const logorepo={
    'Github':'cdn/image/github.png',
    'Gitlab':'cdn/image/gitlab.png',
    'Medium':'cdn/image/medium.png',
    'Website':'cdn/image/web.png'
}
