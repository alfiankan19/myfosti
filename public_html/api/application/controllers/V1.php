<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V1 extends CI_Controller {
    function __construct(){
        parent::__construct();
        //$this->load->helper(array('form', 'url'));
    }
	public function index()
	{
		

            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode(array(
                    'text' => 'Error 200',
                    'type' => 'Welcome to Fosti API'
            )));


    }
    
    public function getall(){
        $data=array(
            'judul' => 'Drone Fosti',
            'description' => 'Pesawat tanpa awak atau Pesawat nirawak (EN: Unmanned Aerial Vehicle atau disingkat UAV atau sering disebut sebagai dron), adalah sebuah mesin terbang yang berfungsi dengan kendali jarak jauh oleh pilot atau mampu mengendalikan dirinya sendiri, menggunakan hukum aerodinamika untuk mengangkat dirinya, bisa digunakan kembali dan mampu membawa muatan baik senjata maupun muatan lainnya. Penggunaan terbesar dari pesawat tanpa awak ini adalah di bidang militer, tetapi juga digunakan di bidang geografi, fotografi, dan videografi yang dilakukan secara bebas dan terbuka. Di bidang geografi, pesawat tanpa awak digunakan sebagai salah satu wahana pengindraan jauh yang sangat penting dalam pembuatan peta, seperti peta penggunaan lahan, peta daerah rawan bencana, dan peta daerah aliran sungai. Rudal walaupun mempunyai kesamaan tetapi tetap dianggap berbeda dengan pesawat tanpa awak karena rudal tidak bisa digunakan kembali dan rudal adalah senjata itu sendiri. ',
            'stack' => 'C++ Arduino'
        );


        return $this->output->set_content_type('application/json')->set_status_header(200)->set_output(
            json_encode(
                array(
                'status' => 'Succes 200',
                'f' => 'Welcome to Fosti API'
                )
            )
        );
    }

    public function auth(){
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass){
            $return=array('status' => 'login','role' => $pass->role);
        }else{
            $return=array('status' => 'logout');
        }
        echo json_encode($return);
    }

    public function alluser(){
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass){
            $datauser =$this->db->query("SELECT id,name,image FROM user")->result();
            $return=$datauser;
        }else{
            $return=array('status' => 'logout');
        }
        echo json_encode($return);
    }

    public function cekjudul(){
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $judul = $this->input->post('judul');
        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass){
            $cekjudul = $this->db->query("SELECT * FROM page WHERE judul ='$judul'")->row();
            if($cekjudul){
                $return=array('judul' => 'block');
            }else{
                $return=array('judul' => 'boleh');
            }
            
        }else{
            $return=array('status' => 'logout');
        }
        echo json_encode($return);
    }

    public function newPage(){
        $openid = $this->input->post('openid');
        $id = $this->input->post('id');

        $token = $this->input->post('token');
        
        $gambar = $this->input->post('gambar');
        $judul = $this->input->post('judul');
        $kategori = $this->input->post('kategori');
        $team = $this->input->post('team');

        $team = str_replace('&gt;', 'X', str_replace('&lt;?', 'X', $team));

        $stack = $this->input->post('stack');
        $repo = $this->input->post('repo');
        $email = $this->input->post('email');
        $telp = $this->input->post('telp');
        $mode = $this->input->post('mode');
        $deskripsi = $this->input->post('deskripsi');
        $konten = $this->input->post('konten');

        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass->role==1){
            //do yang bener
            //cek judul
            //$cekjudul = $this->db->query("SELECT * FROM page WHERE judul ='$judul'")->row();
            //if($cekjudul){
                
                if($mode==0){
                //indert data
                    $this->db->query("INSERT INTO page (userid, judul, kategori,team,stack,repo,email,telp,gambar,konten,likes,deskripsi) 
                    VALUES ('$openid','$judul','$kategori','$team','$stack','$repo','$email','$telp','$gambar','$konten','[]','$deskripsi')");
                    $return=array('message' => 'Berhasil Upload','code' => '200');
                }else{
                    $this->db->query("UPDATE page SET judul='$judul', kategori='$kategori',team='$team',stack='$stack',repo='$repo',email='$email',telp='$telp',gambar='$gambar',konten='$konten',deskripsi='$deskripsi' WHERE id=$id");
                    $return=array('message' => 'Berhasil Update','code' => '200');
                }
            //}else{
             //   $return=array('error' => 'judul sudah ada, gunakan judul lain');
            //}
            
        }else{
            $return=array('status' => 'logout');
        }
        echo json_encode($return);
    }

    public function getpage(){
        $url = $this->input->post('url');
        $judu = str_replace('-', ' ', $url);
        $data = $this->db->query("SELECT * FROM page WHERE judul='$judu'")->row();
        if($data){
        $this->db->query("UPDATE page SET view=view+1 WHERE judul='$judu'"); 
        echo json_encode($data);
        }else{
            echo json_encode(array('error' => '404'));
        }
    }
    
    public function listpf(){
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass){
            
            $return=$this->db->query("SELECT * FROM page WHERE userid='$openid'")->result();
        }else{
            $return=array('status' => 'logout');
        }
        echo json_encode($return);
    }

    public function listall(){
        $mulai = $this->input->post('mulai');
        $limit = $this->input->post('limit');
        $keyword = $this->input->post('keyword');
        $kategori = $this->input->post('kategori');
        if($keyword=='' && $kategori==''){
            $return=$this->db->query("SELECT * FROM page ORDER BY id DESC  LIMIT $mulai,$limit")->result();
        }elseif($keyword!='' && $kategori==''){
            $return=$this->db->query("SELECT * FROM page WHERE judul LIKE '%$keyword%' ORDER BY id DESC  LIMIT $mulai,$limit")->result();
        }elseif($keyword=='' && $kategori!=''){
            //kategori all
            $return=$this->db->query("SELECT * FROM page WHERE kategori LIKE '%$kategori%' ORDER BY id DESC  LIMIT $mulai,$limit")->result();
        }elseif($keyword!='' && $kategori!=''){
            //kategori cari
            $return=$this->db->query("SELECT * FROM page WHERE kategori LIKE '%$kategori%' AND judul LIKE '%$keyword%' ORDER BY id DESC  LIMIT $mulai,$limit")->result();
        }
        
  
        echo json_encode($return);
    }
    
    public function delete(){
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $id = $this->input->post('id');
        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass->role==1){

            $this->db->query("DELETE FROM page WHERE id=$id"); 
            $return = array('delete' => '1');


        }else{
            $return=array('status' => 'logout');
        }
        echo json_encode($return);
    }

    public function uploader()
    {

        $nama = $this->input->post('namafile');

        $config['upload_path']          = 'upload';
        $config['allowed_types']        = 'gif|jpg|png';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        $config['file_name']           = $nama.".jpg";
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('gambar'))
        {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode(array('status' => $error));
               
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                echo json_encode($data);
                
        }
    }

    public function uploader2()
    {

        $nama = $this->input->post('namafile');

        $config['upload_path']          = 'upload';
        $config['allowed_types']        = 'gif|jpg|png';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        $config['file_name']           = time().".jpg";
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('gambar'))
        {
                $error = array('error' => $this->upload->display_errors());
                //echo json_encode(array('status' => $error));

                $data=array(
                    'url' => $this->upload->display_errors()
                );
                $json=array(
                    'success' => 0
                );
                echo json_encode($json);
               
        }
        else
        {
                //$data = array('upload_data' => $this->upload->data());
                $data=array(
                    'url' => 'http://localhost/api/upload/'.$config['file_name']
                );
                $json=array(
                    'success' => 1,
                    'file' => $data
                );
                echo json_encode($json);
                
        }
    }

    public function like()
    {
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $json=$this->input->post('like');
        $id=$this->input->post('id');
        $pass = $this->db->query("SELECT * FROM user WHERE openid='$openid'")->row();
        if($pass){
            $this->db->query("UPDATE page SET likes='$json' WHERE id=$id");
        }else{
            $return=array('status' => 'logout');
        }

        
        
    }

    public function daftar()
    {
        $openid = $this->input->post('openid');
        $token = $this->input->post('token');
        $image = $this->input->post('image');
        $name = $this->input->post('name');
        $this->db->query("INSERT INTO user (openid,token,image,name) VALUES ('$openid','$token','$image','$name')");
        echo json_encode(array('status' => 'oke'));
    }

    public function verifikasi()
    {
        $kode = $this->input->get('kode');
        $pass = $this->input->get('pass');
        if($pass=='54535251'){
            $this->db->query("UPDATE user SET role=1 WHERE openid='$kode'");
            echo json_encode(array('status' => 'terverifikasi'));
        }
    }

}
